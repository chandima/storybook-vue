# Storybook Vue
### Sharing reusable Vue.js components with Lerna, Storybook, and npm

https://medium.com/js-dojo/sharing-reusable-vue-js-components-with-lerna-storybook-and-npm-7dc33b38b011

```
$ cd storybook-vue
# install lerna
$ npx lerna init --independent 

# install sotrybook
$ npx -p @storybook/cli sb init --type vue 

# install dependencies
$ npm install vue --save
$ npm install vue-loader vue-template-compiler @babel/core babel-loader babel-preset-vue --save-dev

# rename packages folder to components
# "packages/*" to "components/*" in lerna.json

# follow article to create new JTableRow

# create second component called JTable 
# add JTableRow as its dependency
$ npx lerna add @edplus-ds/j-table-row --scope=@edplus-ds/j-table

```
### Publish packages
```
$ npm login
$ npm run publish
```

### Run local npm registry (Verdaccio)
```
$ docker pull verdaccio/verdaccio
$ docker run -it --rm --name verdaccio -p 4873:4873 verdaccio/verdaccio
$ npm adduser --registry http://0.0.0.0:4873

# set local npm registry in project level .npmrc
$ npm set registry http://0.0.0.0:4873 --userconfig ./.npmrc
```

![Storybook Vue](https://cdn-images-1.medium.com/max/800/1*TMD9hDJJX-MG8HZU9CVxqA.jpeg)