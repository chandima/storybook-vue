# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.3"></a>
## [0.1.3](https://bitbucket.org/projects/chandima/repos/storybook-vue/compare/diff?targetBranch=refs%2Ftags%2F@edplus-ds/j-table-row@0.1.2&sourceBranch=refs%2Ftags%2F@edplus-ds/j-table-row@0.1.3) (2019-06-23)

**Note:** Version bump only for package @edplus-ds/j-table-row
